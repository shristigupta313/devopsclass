resource "aws_subnet" "ninja_pub_sub_01" {
  vpc_id                  = aws_vpc.ninja_vpc_01.id
  cidr_block              = "10.0.0.0/28"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-east-1a"

  tags = {
    Name = "public-subnet"
  }
}

resource "aws_subnet" "ninja_priv_sub_01" {
  vpc_id                  = aws_vpc.ninja_vpc_01.id
  cidr_block              = "10.0.0.16/28"
  map_public_ip_on_launch = "false"
  availability_zone       = "us-east-1a"

  tags = {
    Name = "private-subnet"
  }
}
